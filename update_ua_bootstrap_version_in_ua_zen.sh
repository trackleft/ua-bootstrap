#!/bin/bash

# update_ua_bootstrap_version_in_ua_zen.sh
# Update UA_Quickstart UA_Zen with latest UA Bootstrap version.
#
# This script updates the UA_ZEN_UA_BOOTSTRAP_STABLE_VERSION constant that sets
# UA Zen's version of UA Bootstra with the $NEW_TAG value and then stages,
# commits, and pushes the change.
#
# To work properly, this script must be run from within a cloned working tree of
# the ua_quickstart repository.
#
# Required environment variables
# - $BITBUCKET_TAG The most recent tagged release version.
# - $PREVIOUS_TAG The previous tagged release version.
#

if [ -z "$BITBUCKET_TAG" ]; then
    echo "Please set BITBUCKET_TAG environment variable."
    exit 1
fi

if [ -z "$PREVIOUS_TAG" ]; then
    echo "Please set PREVIOUS_TAG environment variable."
    exit 1
fi

SEARCH_PATTERN="\[${PREVIOUS_TAG}\]"

sed -i "s/${SEARCH_PATTERN}.*/${SEARCH_PATTERN}\ \=\ ${BITBUCKET_TAG}/g" themes/custom/ua_zen/includes/common.inc