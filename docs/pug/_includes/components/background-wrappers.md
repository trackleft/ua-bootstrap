<h2 id="background-wrappers" class="page-header">Background Wrappers</h2>
<p class="lead">Sometimes you just want the background to be full width in certain sections.</p>
<p>For full width backgrounds, you must close the`.row`, and `.container` and then wrap a new `.container` `<div>` in another `<div>` with the `.background-wrapper` class. Since `<div>`s go full width naturally, you'll know when you haven't closed all structural `<div>`s when the wrapper does not span the width of the page.</p>
<h3 id="background-wrapper-basic">Basic example</h3>
<p>By default, the background-wrapper class just adds some `padding` to the top
and bottom of the wrapper `<div>`.</p>
</div></div></div></div>
<div class="example" data-example-id="simple-background-wapper">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="background-wrapper bg-silver-tint">
  <div class="container bs-docs-container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        <h2 class="serif">The University of Arizona is committed to achieving
        full accessibility of all electronic and information technology to
        ensure equitable experiences for everyone</h2>
        </div>
      </div>
      <div class="row show-grid">
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
      </div>
    </div>
  </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close Column-->
  </div><!--Close Row-->
</div><!--Close Container-->
<div class="background-wrapper bg-silver-tint">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
    <div class="row">
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
    </div>
  </div>
</div> <!--Close Wrapper-->
```
<h3 id="background-wrapper-triangle-fade">Triangle Fade</h3>
<p>We have also created some repeating svg based backgrounds you can use. SVGs
can be edited in Adobe Illustrator, or even in your favorite text editor.</p>
</div></div></div></div>
<div class="example" data-example-id="simple-background-wapper">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="background-wrapper bg-triangles-fade bg-ash-tint">
  <div class="container bs-docs-container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        <h2 class="serif">The University of Arizona is committed to achieving
        full accessibility of all electronic and information technology to
        ensure equitable experiences for everyone</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-triangles-fade bg-ash-tint">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<h3 id="background-wrapper-trilines">Tri-lines and border accents</h3>
<p>Dress up blocks and background wrappers with border accents.</p>
<div class="example" data-example-id="block-simple-background-wapper">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="row">
    <div class="col-sm-4">
      <div class="bg-trilines border-top-accent-azurite">
        <h2 class="card-title">Special title treatment</h3>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary btn-hollow">Go somewhere</a>
        <h2 class="card-title">Special title treatment</h3>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary btn-hollow">Go somewhere</a>
      </div>
    </div>
  </div>
</div>
```html
<div class="row">
  <div class="col-sm-4">
    <div class="bg-trilines border-top-accent-azurite">
      <h2 class="card-title">Special title treatment</h3>
      <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
      <a href="#" class="btn btn-primary btn-hollow">Go somewhere</a>
      <h2 class="card-title">Special title treatment</h3>
      <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
      <a href="#" class="btn btn-primary btn-hollow">Go somewhere</a>
    </div>
  </div>
</div>
```

</div></div></div></div>
<div class="example" data-example-id="simple-background-wapper">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="background-wrapper bg-trilines border-top-accent-azurite">
  <div class="container bs-docs-container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        <h2 class="serif">The University of Arizona is committed to achieving
        full accessibility of all electronic and information technology to
        ensure equitable experiences for everyone</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-trilines border-top-accent-azurite">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<h3 id="background-wrapper-catalinas-abstract">Catalinas Abstract</h3>
<p>A representation of the Catalina Mountains, this graphic has vertical fadeout.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
      <div class="background-wrapper bg-catalinas-abstract">
      <div class="container bs-docs-container">
        <div class="row">
          <div class="col-md-9 text-center-not-xs">
            <h2 class="serif">The University of Arizona is committed to achieving
            full accessibility of all electronic and information technology to
            ensure equitable experiences for everyone</h2>
            </div>
          </div>
        </div>
      </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-catalinas-abstract">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<h3 id="background-wrapper-triangle-mosaic">Triangle Mosaic</h3>
<p>Triangle mosaic is translucent so you can select the background color you
would like to use.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
      <div class="background-wrapper bg-triangles-mosaic bg-sky">
      <div class="container bs-docs-container">
        <div class="row">
          <div class="col-md-9 text-center-not-xs">
            <h2 class="serif">The University of Arizona is committed to achieving
            full accessibility of all electronic and information technology to
            ensure equitable experiences for everyone</h2>
            </div>
          </div>
        </div>
      </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-triangles-mosaic bg-sky">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<h3 id="background-wrapper-triangles-top-left">Triangles Top Left</h3>
<p>The top left triangles background image blends into the background color so you can select the background color you
would like to use and the image will be updated automatically.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
      <div class="background-wrapper bg-triangles-top-left bg-red">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-white text-size-h1">Accessibility Standards</h2>
              <p class="lead text-white">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-top-left bg-blue">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-white text-size-h1">Accessibility Standards</h2>
              <p class="lead text-white">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-default btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-top-left bg-white">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-top-left bg-cool-gray">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-top-left bg-warm-gray">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-triangles-top-left bg-cool-gray">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<h3 id="background-wrapper-triangles-top-right">Triangles Top Right</h3>
<p>The top right triangles background image blends into the background color so you can select the background color you
would like to use and the image will be updated automatically.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
      <div class="background-wrapper bg-triangles-top-right bg-red">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-white text-size-h1">Accessibility Standards</h2>
              <p class="lead text-white">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-top-right bg-blue">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-white text-size-h1">Accessibility Standards</h2>
              <p class="lead text-white">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-default btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-top-right bg-white">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-top-right bg-cool-gray">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-top-right bg-warm-gray">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-triangles-top-right bg-cool-gray">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<h3 id="background-wrapper-triangles-centered">Triangles Centered</h3>
<p>The top left triangles background image blends into the background color so you can select the background color you
would like to use and the image will be updated automatically.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
      <div class="background-wrapper bg-triangles-centered bg-red">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-white text-size-h1">Accessibility Standards</h2>
              <p class="lead text-white">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-centered bg-blue">
      <div class="container bs-docs-container">
        <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-white text-size-h1">Accessibility Standards</h2>
              <p class="lead text-white">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-red btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-centered bg-white">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-centered bg-cool-gray">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="background-wrapper bg-triangles-centered bg-warm-gray">
        <div class="container bs-docs-container">
          <div class="row">
            <div class="col-md-9 text-center-not-xs">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <h2 class="sans text-midnight text-size-h1">Accessibility Standards</h2>
              <p class="lead text-midnight">View the accessibility standards followed at the University of Arizona</p>
              <a href="http://itaccessibility.arizona.edu/guidelines/standards" class="btn btn-primary btn-lg">Accessibility Standards</a>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-triangles-centered bg-cool-gray">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<!--
<h3 id="background-wrapper-layered-images">Translucent Triangles</h3>
<p>You can place an image under our translucent triangles. Simply add your own class to your own stylesheet with an image background as in the example below.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
    <div class="background-image-wrapper img-night-sky">
      <div class="background-wrapper bg-triangles-translucent">
      <div class="container bs-docs-container">
        <div class="row">
          <div class="col-md-9 text-center-not-xs">
            <h2 class="serif">
                Don't put text over images.<br><br><br>
            </h2>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```css
.img-night-sky {
    background-image: url('../img/night-sky.jpg');
}
```
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column--
  </div><!--Close row--
</div><!--Close container--
<div class="background-image-wrapper img-night-sky">
  <div class="background-wrapper bg-triangles-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-9 text-center-not-xs">
         Don't put text over images.
        </div>
      </div>
    </div>
  </div> <!--Close wrapper--
</div> <!--Close image wrapper--
<div class="container"> <!-- Restart regular columns --
  <div class="row">
    <div class="col-md-9">
```

<h3 id="background-wrapper-layered-images">Translucent Triangles</h3>
<p>Alternatively, you can add images to the `background-image-wrapper` div
inline, as shown in the example below.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
    <div class="background-image-wrapper img-old-main" style="background-image: url('img/old-main.jpg');">
      <div class="background-wrapper bg-triangles-translucent">
      <div class="container bs-docs-container">
        <div class="row">
          <div class="col-md-9 text-center-not-xs">
            <h2 class="serif">
                Don't put text over images.<br><br><br>
            </h2>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column--
  </div><!--Close row--
</div><!--Close container--
<div class="background-image-wrapper img-old-main" style="background-image: url('img/old-main.jpg');">
  <div class="background-wrapper bg-triangles-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-9 text-center-not-xs">
          Don't put text over images.
        </div>
      </div>
    </div>
  </div> <!--Close wrapper--
</div> <!--Close image wrapper--
<div class="container"> <!-- Restart regular columns --
  <div class="row">
    <div class="col-md-9">
```
-->