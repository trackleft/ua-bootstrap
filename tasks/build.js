const $ = {
  merge: require('merge-stream')
}

module.exports = {
  /* Copy necessary files into `dist` */
  dist: (gulp, config) => () => {
    const css = gulp.src(config.buildDir + '/css/ua-bootstrap*')
      .pipe(gulp.dest(config.distDir + '/css'))
    const js = gulp.src(config.buildDir + '/js/ua-bootstrap*')
      .pipe(gulp.dest(config.distDir + '/js'))

    return $.merge(css, js)
  }
}
